from os.path import isfile
from pathlib import Path

from pandas import read_csv, concat

JOIN_RESULT = 'data'
Path(JOIN_RESULT).mkdir(parents=True, exist_ok=True)

# load datasets
df1 = read_csv('resources/movies.csv')
print('df1 shape:', df1.shape)

df2 = read_csv('overtime/movies.csv')
print('df2 shape:', df2.shape)

# join datasets
dfx = concat([df1, df2])
print('concat shape w/  dups:', dfx.shape)

dfx = dfx[~dfx.duplicated(subset=['id'], keep='last')]
print('concat shape w/o dups:', dfx.shape)

dfx.sort_values(by='id', inplace=True)

# consistency check
for _, item in dfx.iterrows():
    if not isfile(f'{JOIN_RESULT}/images/w342/{item["id"]}.jpg'):
        print(f'{item["id"]:<8} file not found')
        break

# finish
dfx.to_csv(f'{JOIN_RESULT}/movies.csv', index=False)
