from csv import writer
from pathlib import Path
from sys import argv

from tmdbv3api import TMDb
from tmdbv3api.exceptions import TMDbException

from adapters.cinematic import Movie, get_latest_movie_id, CinematicException, PosterException
from adapters.size import Size
from utils import Logger, LoggingLevel

# config
tmdb = TMDb()
tmdb.api_key = argv[1]
tmdb.language = 'en'
tmdb.debug = False
MOVIE_MAX_ID = get_latest_movie_id()
CSV_SAVE_MODE = 'w' if argv[2] == 'w' else int(argv[2])  # 'w' or movie's id to start with

POSTER_SIZE = Size.w342.value
BASE_SAVE_PATH = f'./qwerty'
POSTER_SAVE_PATH = f'{BASE_SAVE_PATH}/images/{POSTER_SIZE}/'
CSV_SAVE_PATH = f'{BASE_SAVE_PATH}/movies.csv'

log = Logger(LoggingLevel.INFO)
Path(POSTER_SAVE_PATH).mkdir(parents=True, exist_ok=True)

# prepare csv
column_names = list(vars(Movie(111, False)).keys())
file = open(file=CSV_SAVE_PATH,
            mode='w' if CSV_SAVE_MODE == 'w' else 'a',
            encoding='utf-8',
            newline='')
csv_writer = writer(file)
if file.tell() == 0:
    csv_writer.writerow(column_names)

# download
i = CSV_SAVE_MODE if CSV_SAVE_MODE != 'w' else 5
while i <= MOVIE_MAX_ID:
    dbg = None
    try:
        movie = Movie(i)
        dbg = movie.to_dict()
        csv_writer.writerow(movie.to_row())
        movie.download_poster(POSTER_SIZE, POSTER_SAVE_PATH)
        log.info(i, 'OK')
    except PosterException:
        log.warn(i, 'Movie does not have poster path.')
    except (CinematicException, TMDbException) as e:
        log.error(i, e)
    finally:
        file.flush()
        log.debug(i, dbg.__str__())
    i += 1

#
file.close()
