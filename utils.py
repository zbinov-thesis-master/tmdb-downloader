from enum import Enum


class LoggingLevel(Enum):
    DEBUG = 0
    INFO = 1
    WARNING = 2
    ERROR = 3


class Logger:

    def __init__(self, level: LoggingLevel):
        self.__level = level.value

    def debug(self, id_: int, msg: str):
        if self.__level <= LoggingLevel.DEBUG.value:
            print(f'[DEBUG]    {id_:<6}  {msg}')

    def info(self, id_: int, msg: str):
        if self.__level <= LoggingLevel.INFO.value:
            print(f'[INFO]     {id_:<6}  {msg}')

    def warn(self, id_: int, msg: str):
        if self.__level <= LoggingLevel.WARNING.value:
            print(f'[WARNING]  {id_:<6}  {msg}')

    def error(self, id_: int, msg: str):
        if self.__level <= LoggingLevel.ERROR.value:
            print(f'[ERROR]    {id_:<6}  {msg}')
