from typing import List, Dict

from requests import get
from tmdbv3api import Movie as TmdbMovie, Genre as TmdbGenre
from tmdbv3api.as_obj import AsObj


class Movie:
    POSTER_BASE_URL: str = 'https://image.tmdb.org/t/p/'
    GENRE_OFFICIAL_IDS: List[Dict] = None

    def __init__(self, movie_id: int, enable_rules: bool = True):
        if Movie.GENRE_OFFICIAL_IDS is None:
            Movie.GENRE_OFFICIAL_IDS = list(map(lambda genre: dict(genre), TmdbGenre().movie_list()))

        details = TmdbMovie().details(movie_id, append_to_response='')
        self.id: int = details['id']
        self.genres: List[Genre] = convert_to_genres(details['genres'])
        self.original_title: str = details['original_title']
        self.title: str = details['title']

        self.release_date: str = details['release_date']
        self.budget: int = details['budget']
        self.revenue: int = details['revenue']
        self.vote_average: float = details['vote_average']

        self.vote_count: int = details['vote_count']
        self.original_language: str = details['original_language']
        self.status: str = details['status']
        self.poster_path: str = details['poster_path']
        self.adult: bool = details['adult']

        if enable_rules:
            broken_rules = self.is_valid()
            if not len(broken_rules) == 0:
                raise CinematicException(f'Movie with specified id does not comply with the rules: {broken_rules}')

    def download_poster(self, size: str, save_path: str) -> None:
        try:
            url = self.POSTER_BASE_URL + size + self.poster_path
            image = get(url).content
            with open(f'{save_path}{self.id}.jpg', 'wb') as file:
                file.write(image)
        except TypeError:
            raise PosterException("Problem downloading the movie poster.")

    # 5-215830 vote_count >= 100
    # 215831-? vote_count >= 10
    def is_valid(self) -> str:
        broken_rules = []
        if not (self.poster_path is not None):
            broken_rules.append('poster_path is not None')
        if not (len(self.genres) > 0):
            broken_rules.append('len(genres) > 0')
        if not (self.adult is False):
            broken_rules.append('adult is False')
        if not (self.vote_count >= 10):
            broken_rules.append('vote_count >= 10')
        return ', '.join(broken_rules)

    def to_row(self) -> List[str]:
        return [var if not isinstance(var, List) else genres_to_ids(var) for var in vars(self).values()]

    def to_dict(self) -> dict:
        result = {}
        for attr in vars(self).items():
            if isinstance(attr[1], List):
                result[attr[0]] = [item.__dict__ for item in attr[1]]
            else:
                result[attr[0]] = attr[1]
        return result

    def __str__(self) -> str:
        result = ''
        for attr in vars(self).items():
            if isinstance(attr[1], List):
                result += f'{attr[0]:<25}{genres_to_ids(attr[1]).__str__()}\n'
            else:
                result += f'{attr[0]:<25}{attr[1].__str__()}\n'
        return result.strip()


class Genre:

    def __init__(self, obj: AsObj):
        self.id: int = obj['id']
        self.name: str = obj['name']

    def __str__(self) -> str:
        result = ''
        for attr in vars(self).items():
            result += f'{attr[0]:<25}{attr[1].__str__()}\n'
        return result.strip()


class CinematicException(Exception):
    pass


class PosterException(CinematicException):
    pass


def get_latest_movie_id() -> int:
    return TmdbMovie().latest()['id']


def convert_to_genres(obj: List[AsObj]) -> List[Genre]:
    return [Genre(item) for item in obj if dict(item) in Movie.GENRE_OFFICIAL_IDS]


def genres_to_names(genres: List[Genre]) -> List[str]:
    return list(map(lambda genre: genre.name, genres))


def genres_to_ids(genres: List[Genre]) -> List[str]:
    return list(map(lambda genre: genre.id, genres))
